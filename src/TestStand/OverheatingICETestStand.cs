﻿using System;


namespace SimulationTestingEngine {

    class OverheatingICETestStand : AbstractTestStend, ITestStand, IOverheatingStend {

        readonly IControlICE _currentEngine;        

        public float TemperatureEngine { get { return _currentEngine.TemperatureEngine; } }

        public float MaxRunningTime { private get; set; }

        public float TotalRunningTime { get; private set; }

        int _countsCycles = 0;
        
        private float TemperatureOverheating { get; set; }


        public OverheatingICETestStand(ConfigForOverheatingStand config) {

            TemperatureOverheating = config.TemperatureOverheating;
            MaxRunningTime = config.MaxRunningTime;
            
            _currentEngine = config.SelectEngine;
            _currentEngine.UpdateStateEngine += new Action(OnUpdateStateEngine);
        }



        public void TransferExternalParameter(string extParameter) {

            try {                
                float inputTemperature = (float)Convert.ToDouble(extParameter);

                if (inputTemperature < GenericConst.MIN_OUTSIDE_TEMPERATURE ||
                    inputTemperature > GenericConst.MAX_OUTSIDE_TEMPERATURE) {

                    GenerateEventIncorrectDataInput(this, new InputError(InputError.TypeErr.IncorrectRange));
                }
                else {
                    _currentEngine.TemperatureOutside = inputTemperature;
                }                    
            }
            catch (FormatException){
                GenerateEventIncorrectDataInput(this, new InputError(InputError.TypeErr.IncorrectFormat));
            }
        }


        public override void StartTest() {

            _countsCycles = 0;

            GenerateEventTestStateUpdated(this, new TestStatus(TestStatus.State.TestStarted));
            _currentEngine.StartEngine();
        }


        protected override void StopTest() {
            _currentEngine.StopEngine();
        }        


        void OnUpdateStateEngine() {

            TotalRunningTime = _countsCycles * GenericConst.QUANT_TIME_SIMULATION_S;
            
            if (TotalRunningTime == 0 && _currentEngine.TemperatureEngine >= TemperatureOverheating) {
                StopTest();
                GenerateEventTestStateUpdated(this, new TestStatus(TestStatus.State.TestInterruptedBeforeToStart));
            }
            else if (_currentEngine.TemperatureEngine >= TemperatureOverheating) {               
                StopTest();
                GenerateEventTestStateUpdated(this, new TestStatus(TestStatus.State.TestComplete));
            }
            else if(TotalRunningTime >= MaxRunningTime) {
                StopTest();
                GenerateEventTestStateUpdated(this, new TestStatus(TestStatus.State.TestInterruptedForOverTime));
            }

            ++_countsCycles;
        }
    }
}
