﻿using System;


namespace SimulationTestingEngine {
    
    abstract class AbstractTestStend {
        
        public event IncorrectDataInput IncorrectInput;
        public event TestStateUpdated TestUpdate;

        public abstract void StartTest();
        protected abstract void StopTest();

        protected void GenerateEventIncorrectDataInput(Object sender,InputError typeError) {
            IncorrectInput?.Invoke(sender, typeError);
        }

        protected void GenerateEventTestStateUpdated(Object sender, TestStatus currentStateTest) {

            TestUpdate?.Invoke(sender, currentStateTest);
        }
    }
}
