﻿

namespace SimulationTestingEngine {
    
    interface ITestStand {

        event IncorrectDataInput IncorrectInput;

        event TestStateUpdated TestUpdate;
        
        float MaxRunningTime { set; }
        float TotalRunningTime { get; }

        void TransferExternalParameter(string extParameter);
        void StartTest();
    }
}
