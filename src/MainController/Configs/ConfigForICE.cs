﻿

namespace SimulationTestingEngine {
    
    class ConfigForICE {

        public IConstantsForICE BasedConstParameters { get; private set; }
        void InitBasedConstParameters() {

            float I, Hm, Hv, C;
            I = 10;
            Hm = 0.01F;
            Hv = 0.0001F;
            C = 0.1F;

            BasedConstParameters = new ConstantParametersForICE(I, Hm, Hv, C);
        }

        public ICalculatorTorqueFromSpeed BasedTorqueChart { get; private set; }
        void InitTorqueChart() {

            float[] M = { 20, 75, 100, 105, 75, 0 };
            float[] V = { 0, 75, 150, 200, 250, 300 }; 

            BasedTorqueChart = new WorkTorqueChart(M, V);
        }
        

        public ConfigForICE() {

            InitBasedConstParameters();           
            InitTorqueChart();
        }
    }
}
