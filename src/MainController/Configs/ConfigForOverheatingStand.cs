﻿

namespace SimulationTestingEngine {
    
    class ConfigForOverheatingStand {

        float T_overheating = 110;

        float Time_maxRunningWorkingS = 36000;

        public IControlICE SelectEngine { get; private set; }
        public float TemperatureOverheating { get; private set; }
        public float MaxRunningTime { get; private set; }
        

        public ConfigForOverheatingStand(IControlICE targetEngine) {

            TemperatureOverheating = T_overheating;
            SelectEngine = targetEngine;
            MaxRunningTime = Time_maxRunningWorkingS;
        }
    }
}
