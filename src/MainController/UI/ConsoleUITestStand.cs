﻿using System;


namespace SimulationTestingEngine {
	
	class ConsoleUITestStand : AbstractUITestStand, ITestStendUI {

		
		public ConsoleUITestStand(ITestStand currentTestStand) : base(currentTestStand) {
			Console.WriteLine("Testing engine simulator v1.0");
		}


		public void RunNewSimulation() {

			Console.WriteLine();
			Console.WriteLine("Test stand ready");
			Console.WriteLine("Input outside temperature value and press Enter");
			Console.Write("OutsideTemperature = ");
			
			string inputParameter = Convert.ToString(Console.ReadLine());
			_currentTestStand.TransferExternalParameter(inputParameter);
			_currentTestStand.StartTest();
		}


		protected override void OnTestStateUpdated(Object sender, TestStatus currentTest) {

			switch (currentTest.Status) {

				case TestStatus.State.TestStarted:
					ShowMessageForStartTest();
					return;				
				case TestStatus.State.TestComplete:
					ShowMessageForCompleteTest(sender);					
					break;
				case TestStatus.State.TestInterruptedForOverTime:
					ShowMessageForOverTimeInterrruptTest(sender);					
					break;
				case TestStatus.State.TestInterruptedBeforeToStart:
					ShowMessageForTestInterruptedBeforeToStart();					
					break;
			}

			RunNewSimulation();
		}

		void ShowMessageForStartTest() {
			
			Console.WriteLine();
			Console.WriteLine("Testing engine started");
		}

		void ShowMessageForCompleteTest(Object stand) {

			
			if (stand is ITestStand) {
				 
				ITestStand sourceData = (ITestStand)stand;

				Console.WriteLine();
				Console.WriteLine("Test result:");
				Console.WriteLine("Time to overheat = " + sourceData.TotalRunningTime + " s");
				Console.WriteLine();
			}
			else {
				Console.WriteLine("Current UI interface not supported current test stand");
			}
		}

		void ShowMessageForOverTimeInterrruptTest(Object stand) {

			ITestStand sourceData = (ITestStand)stand;

			Console.WriteLine();
			Console.WriteLine("Test result:");
			Console.WriteLine("Maximum time for testing exceeded! " +
								"Engine not overheat for current outside temperature.");
			Console.WriteLine("Total time running test = " + sourceData.TotalRunningTime/60/60 + " ch");
			Console.WriteLine();
		}

		void ShowMessageForTestInterruptedBeforeToStart() {

			Console.WriteLine("Engine overheated before started test!");
		}

		protected override void OnIncorrectDataInput(Object sender, InputError error) {

			switch (error.TypeCurrentError) {

				case InputError.TypeErr.IncorrectFormat:
					ShowMessageForIncorrectFormatInput();
					break;
				case InputError.TypeErr.IncorrectRange:
					ShowMessageForIncorrectRangeInput();
					break;
			}

			RunNewSimulation();
		}

		void ShowMessageForIncorrectFormatInput() {

			Console.WriteLine();
			Console.WriteLine("Incorrect format or range input value");
			Console.WriteLine("input float: 'x,y' or 'x' or 'x,0'");
		}
		
		void ShowMessageForIncorrectRangeInput() {

			Console.WriteLine();
			Console.WriteLine("Incorrect range input value");
			Console.WriteLine("input correct value between " + GenericConst.MIN_OUTSIDE_TEMPERATURE +
								" and " + GenericConst.MAX_OUTSIDE_TEMPERATURE);
		}
	}
}

