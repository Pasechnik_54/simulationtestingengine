﻿using System;


namespace SimulationTestingEngine {
    
    abstract class AbstractUITestStand {

		protected ITestStand _currentTestStand;

		public AbstractUITestStand(ITestStand targetStand) {

			_currentTestStand = targetStand;
			SubscribeForTestStandEvents(_currentTestStand);
		}

		void SubscribeForTestStandEvents(ITestStand targetStand) {
			
			targetStand.IncorrectInput += new IncorrectDataInput(OnIncorrectDataInput);
			targetStand.TestUpdate += new TestStateUpdated(OnTestStateUpdated);
		}
		
		protected abstract void OnIncorrectDataInput(Object sender,	InputError error);
		protected abstract void OnTestStateUpdated(Object sender, TestStatus currentStateTest);
	}
}
