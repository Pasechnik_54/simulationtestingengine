﻿

namespace SimulationTestingEngine {
    
    public static class GenericConst {

        public const float MIN_OUTSIDE_TEMPERATURE = -80;
        public const float MAX_OUTSIDE_TEMPERATURE = +130;

        public const float QUANT_TIME_SIMULATION_S = 1;
    }
}
