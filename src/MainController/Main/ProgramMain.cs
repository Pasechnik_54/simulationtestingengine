﻿using System;
using System.Text;


namespace SimulationTestingEngine {

    class ProgramMain {
        
        static void Main(string[] args) {

            Console.OutputEncoding = Encoding.Unicode;

            ISimulationControl currentSimulation = new SimulationConfigurator();
            currentSimulation.InitAndRunSimulation();
        }
    }   
}
