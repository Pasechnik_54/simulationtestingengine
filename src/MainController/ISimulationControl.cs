﻿

namespace SimulationTestingEngine {
    
    interface ISimulationControl {

        void InitAndRunSimulation();
    }
}
