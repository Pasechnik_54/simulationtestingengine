﻿

namespace SimulationTestingEngine {
    
    class SimulationConfigurator : ISimulationControl {

        readonly ITestStendUI _currentUI;
        readonly ITestStand _currentTestStand;
        readonly IControlICE _currentEngine;


        public SimulationConfigurator() {

            ConfigForICE configEng = new ConfigForICE();               
            _currentEngine = new InternalCombustionEngine(configEng);

            ConfigForOverheatingStand configStand = new ConfigForOverheatingStand(_currentEngine);
            _currentTestStand = new OverheatingICETestStand(configStand);
            
            _currentUI = new ConsoleUITestStand(_currentTestStand);
        }
        
        

        public void InitAndRunSimulation() {
            _currentUI.RunNewSimulation();
        }
    }
}
