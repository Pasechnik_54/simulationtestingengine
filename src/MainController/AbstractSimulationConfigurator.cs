﻿

namespace SimulationTestingEngine {
    
    abstract class AbstractSimulationConfigurator {

        public abstract void InitAndRunSimulation();
    }
}
