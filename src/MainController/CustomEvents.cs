﻿using System;


namespace SimulationTestingEngine {

    delegate void IncorrectDataInput(Object sender, InputError typeCurrentError);

    class InputError : EventArgs {

        public enum TypeErr {
            None,
            IncorrectFormat,
            IncorrectRange
        }

        public TypeErr TypeCurrentError { get; private set; }

        public InputError(TypeErr typeError) {
            TypeCurrentError = typeError;
        }
    }


    delegate void TestStateUpdated(Object sender, TestStatus testStatus);

    class TestStatus : EventArgs {

        public enum State {
            TestStarted,
            TestComplete,
            TestInterruptedForOverTime,
            TestInterruptedBeforeToStart
        }

        public State Status { get; private set; }

        public TestStatus(State currentStateTest) {

            Status = currentStateTest;
        }
    }
}
