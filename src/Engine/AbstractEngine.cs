﻿using System;

namespace SimulationTestingEngine {
    
     abstract class AbstractEngine : IControlICE {

        protected ISimulationWorkingEngine _processWorking;

        public bool EngineWorking { get; protected set; } 
        
        public float TemperatureOutside { get; set; }
        public float TemperatureEngine { get; set; }
        
        public event Action UpdateStateEngine;

        public virtual void StartEngine() {            
                EngineWorking = true;
        }

        public virtual void StopEngine() {
            EngineWorking = false;
        }

        public void GenerateEventEngineUpdateState() {
            UpdateStateEngine?.Invoke();
        }
    }
}
