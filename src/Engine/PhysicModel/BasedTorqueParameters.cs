﻿using System;


namespace SimulationTestingEngine {
    
    class BasedTorqueParameters : IPointTorqueForEngineSpeed {
        
        protected float _torque = 0;      // M (Н*м)
        public float Torque {

            get { return _torque; }

            set {

                _torque = value;
                if (_torque < 0) {
                    throw new Exception("Torque negative! Overload!");
                }
            }
        }

        protected float _speedRotation = 0;     // V (радиан/сек)
        public float Speed {

            get { return _speedRotation; }

            set {

                if (value >= 0) {
                    _speedRotation = value;
                }
                else {
                    throw new Exception("Engine rotated reverse direction !!");
                }
            }
        }


        public BasedTorqueParameters() {

            Torque = 0;
            Speed = 0;
        }

        public BasedTorqueParameters(float torque, float speedRotation) {

            Torque = torque;
            Speed = speedRotation;
        }
    }
}
