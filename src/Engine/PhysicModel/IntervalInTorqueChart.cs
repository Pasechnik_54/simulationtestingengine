﻿using System;


namespace SimulationTestingEngine {
    
    class IntervalInTorqueChart : ICalculatorTorqueFromSpeed, IRangeIntervalTorque {

        public IPointTorqueForEngineSpeed PointMin { get; private set; }
        public IPointTorqueForEngineSpeed PointMax { get; private set; }

        float _mul;    // множитель линейной функции
        float _offset;    // смещение линейной функции


        public IntervalInTorqueChart(IPointTorqueForEngineSpeed minPointTorqueChart,
                                IPointTorqueForEngineSpeed maxPointTorqueChart) {

            PointMin = minPointTorqueChart;
            PointMax = maxPointTorqueChart;

            CalcOfLinearParameters();
        }   

        public IntervalInTorqueChart() {

            PointMin = null;
            PointMax = null;
        }


        void CalcOfLinearParameters() {

            CalcOfMulLinearFunction();
            CalcOfOffsetLinearFunction();
        }
        // k = (torqMax - torqMin) / (speedMax - speedMin)
        void CalcOfMulLinearFunction() {

            float deltaTorq = PointMax.Torque - PointMin.Torque;
            float deltaSpeed = PointMax.Speed - PointMin.Speed;

            if (deltaSpeed != 0) {
                _mul = deltaTorq / deltaSpeed;
            }
            else {
                throw new Exception("Incorrect input torque chart Vmin = Vmax");
            }
        }
        // b = torqMin - k*speedMin
        void CalcOfOffsetLinearFunction() {

            _offset = PointMin.Torque - (_mul * PointMin.Speed);
        }


        // M = k*V + b;
        public float CalcOfTorqueFromSpeedRotation(float currentSpeedRotation) {
            
            return (_mul * currentSpeedRotation) + _offset;
        }
    }
}
