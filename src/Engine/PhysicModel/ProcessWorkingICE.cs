﻿using System;


namespace SimulationTestingEngine {
    
    class ProcessWorkingICE : ISimulationWorkingEngine {

        IPhysicalParametersICE _engine;
        IUpdateEngineState _engineUpdate;

        public ProcessWorkingICE(InternalCombustionEngine currentEngine) {
            _engine = currentEngine;
            
            if (currentEngine is IUpdateEngineState) {
                _engineUpdate = currentEngine;
            }
            else {
                throw new Exception("Engine not contain interface UpdateState!");
            }
        }



        public void RunProcessToEngineWorking() {

            while (_engine.EngineWorking) {
                WorkCycle();
            }
        }

        void WorkCycle() {

            UpdateCurrentSpeed();
            UpdateCurrentTorque();
            UpdateCurrentAccelCrank();
            UpdateTemperatureEngine();
           
            _engineUpdate.EngineStateUpdate();
        }


        // V (радиан/сек) = V0 + a*time
        void UpdateCurrentSpeed() {
            _engine.CurrentRotate.Speed += ChangeInTime(_engine.CurrentRotate.CurrentAccelCrank, 
                                                                    GenericConst.QUANT_TIME_SIMULATION_S);
        }


        // M (Н*м) зависит от V
        void UpdateCurrentTorque() {

            _engine.CurrentRotate.Torque = _engine.TorqueChart.CalcOfTorqueFromSpeedRotation(_engine.CurrentRotate.Speed);
        }


        // a = M/I
        void UpdateCurrentAccelCrank() {

            if (_engine.Constant.InertialMoment != 0) {
                _engine.CurrentRotate.CurrentAccelCrank = _engine.CurrentRotate.Torque / _engine.Constant.InertialMoment;
            }
        }


        void UpdateTemperatureEngine() {

            float integerSpeedChangeTemp = SpeedHeatEngine() + SpeedCoolEngine();

            CalcChangeTemp(integerSpeedChangeTemp);
        }

        //Скорость нагрева  Vh = M × Hm + V^2 × Hv(°C/сек)
        float SpeedHeatEngine() {

            float speedHeatFromTorque = _engine.CurrentRotate.Torque * _engine.Constant.HeatingMulFromTorque;            
            float speedHeatFromEngineSpeed = GetSpeedSquared() * _engine.Constant.HeatingMulFromSpeedRotation;

            float integratedSpeedHeat = speedHeatFromTorque + speedHeatFromEngineSpeed;

            return integratedSpeedHeat;
        }

        float GetSpeedSquared() {
            return _engine.CurrentRotate.Speed * _engine.CurrentRotate.Speed;
        }

        //Скорость охлаждения Vc = C × (Tсреды - Тдвигателя) (°C/сек)
        float SpeedCoolEngine() {

            float deltaTemp =  _engine.TemperatureOutside - _engine.TemperatureEngine;
            float speedCoolFromTemperature = _engine.Constant.CoolingMulFromTemperature * deltaTemp;

            return speedCoolFromTemperature;
        }

        void CalcChangeTemp(float speedTempChange) {

            _engine.TemperatureEngine += ChangeInTime(speedTempChange, 
                                                        GenericConst.QUANT_TIME_SIMULATION_S);
        }

        float ChangeInTime(float parameter, in float deltaTime) {
            return parameter * deltaTime;
        }
    }
}
