﻿using System;
using System.Collections.Generic;


namespace SimulationTestingEngine {
    
    class WorkTorqueChart : ICalculatorTorqueFromSpeed {

        List<IRangeIntervalTorque> _workIntervals = new List<IRangeIntervalTorque>();
        
        
        public WorkTorqueChart(float[] torquePoints, float[] speedRotatePoints) {

            if (torquePoints.Length != speedRotatePoints.Length) {
                throw new Exception("Incorrect input data TorqueChart: length of array different");
            }
            else {
                for (int i = 0; i < torquePoints.Length - 1; i++) {
                    
                    IPointTorqueForEngineSpeed minPointInterval = new BasedTorqueParameters();
                    IPointTorqueForEngineSpeed maxPointInterval = new BasedTorqueParameters();

                    SetPointInterval(minPointInterval, i);
                    SetPointInterval(maxPointInterval, i + 1);

                    _workIntervals.Add(new IntervalInTorqueChart(minPointInterval, maxPointInterval));
                }
            }
                       
            void SetPointInterval(IPointTorqueForEngineSpeed targetPoint, int targetIndex) {

                targetPoint.Torque = torquePoints[targetIndex];
                targetPoint.Speed = speedRotatePoints[targetIndex];
            }
        }



        ICalculatorTorqueFromSpeed _currentInterval = new IntervalInTorqueChart();

        public float CalcOfTorqueFromSpeedRotation(float currentSpeedRotation) {

            GetCurrentIntervalTorqueChart(currentSpeedRotation);

            return _currentInterval.CalcOfTorqueFromSpeedRotation(currentSpeedRotation); ;
        }

        void GetCurrentIntervalTorqueChart( float currentSpeed) {

           foreach (IRangeIntervalTorque i in _workIntervals) {

                if (currentSpeed >= i.PointMin.Speed &&
                    currentSpeed < i.PointMax.Speed) {
                    
                    _currentInterval = i as ICalculatorTorqueFromSpeed;
                    break;
                }
           }

           if(_currentInterval == null) {
                throw new Exception("No found target interval in torque chart!");
           }
        }
    }
}
