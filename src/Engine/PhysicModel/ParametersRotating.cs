﻿

namespace SimulationTestingEngine {
    
    class ParametersRotating : BasedTorqueParameters, IParametersRotationForICE {        

        public float CurrentAccelCrank { get; set; } // a = M/I

        public ParametersRotating(float torque, float speedRotation, float acceleration) : base(torque, speedRotation) {
            CurrentAccelCrank = acceleration;
        }
    }
}
