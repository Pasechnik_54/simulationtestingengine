﻿using System;


namespace SimulationTestingEngine {
    
    class ConstantParametersForICE : IConstantsForICE {

        float _inertialMoment = 0;                  // I (Кг*м^2)
        public float InertialMoment { 
            
            get { return _inertialMoment; }

            set {
                if (value > 0) {
                    _inertialMoment = value;
                }
                else {
                    throw new Exception("Incorrect InercialMoment!");
                }
            }
        }


        public float HeatingMulFromTorque { get; private set; }  // Hm (°C/Н*м*сек)

        public float HeatingMulFromSpeedRotation { get; private set; } // Hv (°C*сек/рад^2)

        public float CoolingMulFromTemperature { get; private set; } // C (1/сек)

        
        
        public ConstantParametersForICE(float inertion, float mulHeatFromTorque,
                                        float mulHeatFromSpeedRotate, float mulCoolForTemp) {

            InertialMoment = inertion;
            HeatingMulFromTorque = mulHeatFromTorque;
            HeatingMulFromSpeedRotation = mulHeatFromSpeedRotate;
            CoolingMulFromTemperature = mulCoolForTemp;
        }
    }
}
