﻿

namespace SimulationTestingEngine {
    
    interface IParametersRotationForICE : IPointTorqueForEngineSpeed {

        float CurrentAccelCrank { get; set; }
    }
}
