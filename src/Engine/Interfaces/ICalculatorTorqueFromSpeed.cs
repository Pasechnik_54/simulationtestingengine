﻿

namespace SimulationTestingEngine {
    
    interface ICalculatorTorqueFromSpeed {

        float CalcOfTorqueFromSpeedRotation(float currentSpeedRotation);
    }
    
}
