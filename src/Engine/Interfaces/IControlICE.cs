﻿using System;


namespace SimulationTestingEngine {
    
     interface IControlICE {

        event Action UpdateStateEngine;

        float TemperatureOutside { get;  set; }
        float TemperatureEngine { get; }

        void StartEngine();
        void StopEngine();
    }
}
