﻿

namespace SimulationTestingEngine {
    
    interface IPointTorqueForEngineSpeed {

        float Speed { get; set; }
        float Torque { get; set; }
    }
}
