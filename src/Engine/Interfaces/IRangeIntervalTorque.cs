﻿

namespace SimulationTestingEngine {

    interface IRangeIntervalTorque {

        IPointTorqueForEngineSpeed PointMin { get; }
        IPointTorqueForEngineSpeed PointMax { get; }
    }
}
