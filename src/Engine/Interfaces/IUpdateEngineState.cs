﻿

namespace SimulationTestingEngine {
    
    interface IUpdateEngineState {

        void EngineStateUpdate();
    }
}
