﻿

namespace SimulationTestingEngine {
    
    interface IConstantsForICE {

        float InertialMoment { get; }
        float HeatingMulFromTorque { get; }
        float HeatingMulFromSpeedRotation { get; }
        float CoolingMulFromTemperature { get; }
    }
}
