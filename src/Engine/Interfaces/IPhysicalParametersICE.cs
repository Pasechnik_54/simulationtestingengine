﻿

namespace SimulationTestingEngine {
    
    interface IPhysicalParametersICE {

        bool EngineWorking { get; }
        float TemperatureEngine { get; set; }
        float TemperatureOutside { get; }
        
        IConstantsForICE Constant { get; }
        ICalculatorTorqueFromSpeed TorqueChart { get; }
        IParametersRotationForICE CurrentRotate { get; set; }
    }
}
