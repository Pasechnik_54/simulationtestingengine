﻿

namespace SimulationTestingEngine {
    
    class InternalCombustionEngine : AbstractEngine, IControlICE, IPhysicalParametersICE,
                                                                        IUpdateEngineState {
                
        public IConstantsForICE Constant { get; private set; }
        public ICalculatorTorqueFromSpeed TorqueChart { get; private set; }
        public IParametersRotationForICE CurrentRotate { get; set; }
       

        public InternalCombustionEngine(ConfigForICE currentConfig) {

            TemperatureEngine = TemperatureOutside;

            Constant = currentConfig.BasedConstParameters;
            TorqueChart = currentConfig.BasedTorqueChart;                      
            CurrentRotate = new ParametersRotating(0, 0, 0);

            _processWorking = new ProcessWorkingICE(this);
        }     



        public override void StartEngine() {

            TemperatureEngine = TemperatureOutside;

            base.StartEngine();            
            _processWorking.RunProcessToEngineWorking();
        }
        
        public  override void StopEngine() {

            StoppedRotationEngine();
            base.StopEngine();
        }

        void StoppedRotationEngine() {
            
            CurrentRotate.Torque = 0;
            CurrentRotate.Speed = 0;
            CurrentRotate.CurrentAccelCrank = 0;
        }

        public void EngineStateUpdate() {
            GenerateEventEngineUpdateState();
        }
    }
}
